package com.edu.hbwe.book.dao;



import java.util.List;

import com.edu.hbwe.book.entity.Notice;

public interface NoticeDao {
	//前台系统，查询最新添加或修改的一条公告
	public Notice getRecentNotice();
	//后台系统，查询所有的公告
	public List<Notice> getAllNotices();
	//后台系统，添加公告
	public void addNotice(Notice notice) ;

	//后台系统，根据id修改单个公告
	public void updateNotice(Notice bean);
	//后台系统，根据id查找公告
	public Notice findNoticeById(String n_id);
	//后台系统，根据id删除公告
	public void deleteNotice(String n_id);



	

	

	
}
