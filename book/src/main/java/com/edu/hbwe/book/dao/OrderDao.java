package com.edu.hbwe.book.dao;

import java.sql.SQLException;
import java.util.List;

import com.edu.hbwe.book.entity.Order;
import com.edu.hbwe.book.entity.User;

public interface OrderDao {
	// 查找所有订单
	public List<Order> findAllOrder();
    //条件查询订单
	public List<Order> findOrderByManyCondition(String id, String receiverName);
	//根据id查看单个订单
	public Order findOrderById(String id);
	
	public void addProduct(Order order) throws SQLException;
	
	public int updateOrderState(String orderid);
	
	public List<Order> findOrderByUser(User user);
	
	public void delOrderById(String id) throws SQLException;

}
