package com.edu.hbwe.book.dao;

import java.sql.SQLException;
import java.util.List;

import com.edu.hbwe.book.entity.Order;
import com.edu.hbwe.book.entity.OrderItem;

public interface OrderItemDao {

	public void addOrderItem(Order order) throws SQLException;

	public List<OrderItem> findOrderItemByOrder(Order order);

	public void delOrderItems(String id) throws SQLException;


}
