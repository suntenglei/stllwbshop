package com.edu.hbwe.book.dao;

import java.sql.SQLException;
import java.util.List;

import com.edu.hbwe.book.entity.Order;
import com.edu.hbwe.book.entity.Product;

public interface ProductDao {
    //后台添加商品
	public int addProduct(Product p) ;

	public List<Product> listAll() ;

	public List<Object[]> getWeekHotProduct() ;

	public int deleteProduct(String id) ;

	public Product findProductById(String id);

	public int findAllCount(String category) ;

	public List<Product> findByPage(int currentPage, int currentCount, String category) ;

	public int findBookByNameAllCount(String searchfield) ;

	public List<Product> findBookByName(int currentPage, int currentCount, String searchfield) ;

	public List<Product> findProductByManyCondition(String id, String name, String category, String minprice,
			String maxprice);

	public int editProduct(Product p) ;

	public List<Object[]> salesList(String year, String month);

	public void changeProductNum(Order order) throws SQLException;

}
