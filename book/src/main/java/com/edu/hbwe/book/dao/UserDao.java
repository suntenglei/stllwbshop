package com.edu.hbwe.book.dao;


import com.edu.hbwe.book.entity.User;

public interface UserDao {

	void addUser(User user);

	User findUserByActiveCode(String activeCode);

	User findUserByUsernameAndPassword(String username, String password);

	int activeUser(String activeCode);

}
