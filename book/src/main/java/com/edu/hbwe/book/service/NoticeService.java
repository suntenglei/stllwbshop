package com.edu.hbwe.book.service;

import java.util.List;

import com.edu.hbwe.book.entity.Notice;

public interface NoticeService {

	public Notice getRecentNotice();

	public List<Notice> getAllNotices();

	public void addNotice(Notice bean);

	public void updateNotice(Notice bean);

	public Notice findNoticeById(String n_id);

	public void deleteNotice(String n_id);

	



	

}
