package com.edu.hbwe.book.service;

import java.util.List;

import com.edu.hbwe.book.entity.Order;
import com.edu.hbwe.book.entity.User;
import com.edu.hbwe.book.exception.OrderException;

public interface OrderService {
     //查询所有订单
	public List<Order> findAllOrder();
    //条件查询订单
	public List<Order> findOrderByManyCondition(String id, String receiverName);
	//查看单个订单
	public Order findOrderById(String id);
	//管理员删除订单
	public void delOrderById(String id);
	
	public void updateState(String orderid) throws OrderException;
	
	public void addOrder(Order order);
	
	public List<Order> findOrderByUser(User user);
	



}
