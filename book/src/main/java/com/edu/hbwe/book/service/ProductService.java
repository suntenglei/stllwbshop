package com.edu.hbwe.book.service;

import java.util.List;

import com.edu.hbwe.book.entity.PageBean;
import com.edu.hbwe.book.entity.Product;
import com.edu.hbwe.book.exception.AddProductException;
import com.edu.hbwe.book.exception.FindProductByIdException;
import com.edu.hbwe.book.exception.ListProductException;

public interface ProductService {

	public List<Object[]> getWeekHotProduct();

	public List<Product> listAll() throws ListProductException;

	public void deleteProduct(String id);

	public Product findProductById(String ids) throws FindProductByIdException;

	public PageBean findProductByPage(int currentPage, int currentCount, String category);

	public PageBean findBookByName(int currentPage, int currentCount, String searchfield);

	public List<Product> findProductByManyCondition(String id, String name,
			String category, String minprice, String maxprice);

	public void editProduct(Product p);

	public void addProduct(Product p)throws AddProductException;
	
	public List<Object[]> download(String year, String month);

	
}
