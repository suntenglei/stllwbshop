package com.edu.hbwe.book.service;

import javax.security.auth.login.LoginException;

import com.edu.hbwe.book.entity.User;
import com.edu.hbwe.book.exception.ActiveUserException;
import com.edu.hbwe.book.exception.RegisterException;

public interface UserService {

	public User login(String username, String password) throws LoginException;

	public void register(User user) throws RegisterException;

	public void activeUser(String activeCode) throws ActiveUserException;

}
