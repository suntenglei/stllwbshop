package com.edu.hbwe.book.service.impl;
import java.util.List;

import com.edu.hbwe.book.dao.NoticeDao;
import com.edu.hbwe.book.dao.impl.NoticeDaoImpl;
import com.edu.hbwe.book.entity.Notice;
import com.edu.hbwe.book.service.NoticeService;


public class NoticeServiceImpl implements NoticeService {
	private NoticeDao dao = new NoticeDaoImpl();


	public Notice getRecentNotice() {
		Notice recentNotice = dao.getRecentNotice();
		if(recentNotice == null)
			throw new RuntimeException("查询最新添加或修改的一条公告失败！");
			
		return recentNotice;
	}

	//后台系统，查询所有公告
	public List<Notice> getAllNotices() {
		
		List<Notice> listNoList= dao.getAllNotices();
		if (listNoList==null) 
			throw new RuntimeException("查询所有的公告失败！");  
		
		return listNoList;
	    
	}

	//后台系统，添加公告
	public void addNotice(Notice notice) {
		
		dao.addNotice(notice);
		
	}

	//后台系统，根据id修改公告
	public void updateNotice(Notice bean) {
		dao.updateNotice(bean);
	}

	//后台系统，根据id查找公告
		public Notice findNoticeById(String n_id) {
			
		Notice fiNotice= dao.findNoticeById(n_id);
		     if (fiNotice==null) 
		    	 throw new RuntimeException("根据id查找公告失败！");
		     return fiNotice;
				
			
		}

	////后台系统，根据id删除公告
		public void deleteNotice(String n_id) {
			dao.deleteNotice(n_id);
			
		}

}
