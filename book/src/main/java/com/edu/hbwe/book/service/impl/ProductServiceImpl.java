package com.edu.hbwe.book.service.impl;
import java.util.List;

import com.edu.hbwe.book.dao.ProductDao;
import com.edu.hbwe.book.dao.impl.ProductDaoImpl;
import com.edu.hbwe.book.entity.PageBean;
import com.edu.hbwe.book.entity.Product;
import com.edu.hbwe.book.exception.AddProductException;
import com.edu.hbwe.book.exception.FindProductByIdException;
import com.edu.hbwe.book.exception.ListProductException;
import com.edu.hbwe.book.service.ProductService;




public class ProductServiceImpl implements ProductService{

	private ProductDao dao = new ProductDaoImpl();
	// 添加商品
	public void addProduct(Product p) throws AddProductException {
		if(dao.addProduct(p) != 1)
			throw new AddProductException("添加商品失败");
	}
	// 查找所有商品信息
	public List<Product> listAll() throws ListProductException {
		List<Product> all = dao.listAll();
		if(all == null)
			throw new ListProductException("查询商品失败");
		return all;
	}
	//
	public List<Object[]> getWeekHotProduct() {
		List<Object[]> list = dao.getWeekHotProduct();
		if(list == null)
			throw new RuntimeException("前台获取本周热销商品失败！");

		return list;

	}
	public void deleteProduct(String id) {
		if(dao.deleteProduct(id) !=1)
			throw new RuntimeException("后台系统根据id删除商品信息失败！");
	}
	// 根据id查找商品
	public Product findProductById(String id) throws FindProductByIdException {
		Product product = dao.findProductById(id);
		if(product == null)
			throw new FindProductByIdException("根据ID查找商品失败");
		return product;
	}

	public PageBean findProductByPage(int currentPage, int currentCount, String category) {
		PageBean bean = new PageBean();
		// 封装每页显示数据条数
		bean.setCurrentCount(currentCount);
		// 封装当前页码
		bean.setCurrentPage(currentPage);
		// 封装当前查找类别
		bean.setCategory(category);
		// 获取总条数
		int totalCount = dao.findAllCount(category);
		if(totalCount <0)
			throw new RuntimeException("总记录数获取失败");
		bean.setTotalCount(totalCount);
		// 获取总页数
		int totalPage = (int) Math.ceil(totalCount * 1.0 / currentCount);
		bean.setTotalPage(totalPage);
		// 获取当前页数据
		List<Product> ps = dao.findByPage(currentPage, currentCount,
				category);
		if(ps ==null)
			throw new RuntimeException("当前页数据获取失败");
		bean.setPs(ps);
		return bean;
	}

	public PageBean findBookByName(int currentPage, int currentCount, String searchfield) {
		PageBean bean = new PageBean();
		// 封装每页显示数据条数
		bean.setCurrentCount(currentCount);
		// 封装当前页码
		bean.setCurrentPage(currentPage);
		// 封装模糊查询的图书名
		bean.setSearchfield(searchfield);
		// 获取总条数
		int totalCount = dao.findBookByNameAllCount(searchfield);
		if(totalCount <0)
			throw new RuntimeException("总记录数获取失败");
		bean.setTotalCount(totalCount);

		// 获取总页数
		int totalPage = (int) Math.ceil(totalCount * 1.0 / currentCount);
		bean.setTotalPage(totalPage);

		//满足条件的图书
		List<Product> ps = dao.findBookByName(currentPage,currentCount,searchfield);
		if(ps ==null)
			throw new RuntimeException("前台搜索框根据书名查询图书失败！");
		bean.setPs(ps);
		return bean;
	}
	//后台查询
	public List<Product> findProductByManyCondition(String id, String name,
			String category, String minprice, String maxprice) {
		List<Product> ps = null;
		ps = dao.findProductByManyCondition(id, name, category, minprice,maxprice);
		if(ps == null)
			throw new RuntimeException("数据查询失败");
		return ps;
	}
	
	@Override
	public void editProduct(Product p) {
		int row = dao.editProduct(p);
		if(row !=1)
			throw new RuntimeException("编辑商品失败");
	}
	
	@Override
	public List<Object[]> download(String year, String month) {
		List<Object[]> salesList = null;
		salesList = dao.salesList(year, month);
		return salesList;
	}

}
