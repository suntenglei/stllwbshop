package com.edu.hbwe.book.service.impl;
import java.util.Date;
import javax.security.auth.login.LoginException;

import com.edu.hbwe.book.dao.UserDao;
import com.edu.hbwe.book.dao.impl.UserDaoImpl;
import com.edu.hbwe.book.entity.User;
import com.edu.hbwe.book.exception.ActiveUserException;
import com.edu.hbwe.book.exception.RegisterException;
import com.edu.hbwe.book.service.UserService;
import com.edu.hbwe.book.util.MailUtils;



public class UserServiceImpl implements UserService {
	private UserDao dao = new UserDaoImpl();
	// 注册操作
	public void register(User user) throws RegisterException {
		// 调用dao完成注册操作
		try {
			String gender = user.getGender();
			if(gender == "1") {
				user.setGender("男");
			}else {
				user.setGender("女");
			}
			dao.addUser(user);
			 //发送激活邮件 
			   String emailMsg = "感谢您注册网上书城，点击" +
			  "<a href='http://localhost:8080/book/activeUser?activeCode=" +
			  user.getActiveCode() + "'>&nbsp;激活&nbsp;</a>后使用。" +
			  "<br />为保障您的账户安全，请在24小时内完成激活操作"; MailUtils.sendMail(user.getEmail(),emailMsg);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RegisterException("注册失败");
		}
	}
	// 激活用户
	public void activeUser(String activeCode) throws ActiveUserException {
			// 根据激活码查找用户
			User user = dao.findUserByActiveCode(activeCode);
			if (user == null) {
				throw new ActiveUserException("激活用户失败");
			}
			// 判断激活码是否过期 24小时内激活有效.
			// 1.得到注册时间
			Date registTime = user.getRegistTime();
			// 2.判断是否超时
			long time = System.currentTimeMillis() - registTime.getTime();
			if (time / 1000 / 60 / 60 > 24) {
				throw new ActiveUserException("激活码过期");
			}
			// 激活用户，就是修改用户的state状态
			if(dao.activeUser(activeCode) !=1) {
				throw new ActiveUserException("激活用户失败");
			}
		
	}
	// 登录操作
	public User login(String username, String password) throws LoginException {
			//根据登录时表单输入的用户名和密码，查找用户
			User user = dao.findUserByUsernameAndPassword(username, password);
			//如果找到，还需要确定用户是否为激活用户
			if (user != null) {
				// 只有是激活才能登录成功，否则提示“用户未激活”
				if (user.getState() == 1) {
					return user;
				}
				throw new LoginException("用户未激活");
			}
			throw new LoginException("用户名或密码错误");
//			throw new LoginException("登录失败");
	}
}
