package com.edu.hbwe.book.servlet.client;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edu.hbwe.book.exception.ActiveUserException;
import com.edu.hbwe.book.service.UserService;
import com.edu.hbwe.book.service.impl.UserServiceImpl;

/**
 * Servlet implementation class ActiveUserServlet
 */
@WebServlet("/activeUser")
public class ActiveUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 1.获取激活码
		String activeCode = request.getParameter("activeCode");
		// 2.调用service中激活用户操作
		UserService uService = new UserServiceImpl();
		try {
			uService.activeUser(activeCode);
			response.sendRedirect(request.getContextPath() + "/client/activesuccess.jsp");
			return;
		} catch (ActiveUserException e) {
			e.printStackTrace();
			response.getWriter().write(e.getMessage());
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
