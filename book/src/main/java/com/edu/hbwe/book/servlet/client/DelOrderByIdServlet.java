package com.edu.hbwe.book.servlet.client;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edu.hbwe.book.service.OrderService;
import com.edu.hbwe.book.service.impl.OrderServicelmpl;

/**
 * 删除订单
 * @author admin
 *
 */
@WebServlet("/delOrderById")
public class DelOrderByIdServlet extends HttpServlet {
	private static final long serialVersionUID = -742965707205621644L;
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 订单id
		String id = request.getParameter("id");
		// 已支付的订单带有type值为client的参数
		String type = request.getParameter("type");
		OrderService service = new OrderServicelmpl();
		if (type != null && type.trim().length() > 0) {
			//管理员删除订单
			service.delOrderById(id);
			if ("admin".equals(type)) {
				request.getRequestDispatcher("/findOrders").forward(request, response);
				return;
			}
		
		}
		
		request.getRequestDispatcher("/findOrderByUser").forward(request, response);
		return;
	}
}
