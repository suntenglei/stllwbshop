package com.edu.hbwe.book.servlet.client;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edu.hbwe.book.entity.Product;
import com.edu.hbwe.book.exception.FindProductByIdException;
import com.edu.hbwe.book.service.ProductService;
import com.edu.hbwe.book.service.impl.ProductServiceImpl;

/**
 * 根据商品id查找指定商品信息的servlet
 */
@WebServlet("/findProductById")
public class FindProductByIdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String id = request.getParameter("id");
		String type = request.getParameter("type");		
		ProductService service=new ProductServiceImpl();
		try {
			Product p = service.findProductById(id);
			request.setAttribute("p", p);
			if (type == null) {
				request.getRequestDispatcher("/client/info.jsp").forward(request,response);
				return;
			}						
			request.getRequestDispatcher("/admin/products/edit.jsp").forward(request, response);
			return;
		} catch (FindProductByIdException e) {
			e.printStackTrace();
		}
	}
}
