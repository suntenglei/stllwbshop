package com.edu.hbwe.book.servlet.client;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.edu.hbwe.book.entity.Notice;
import com.edu.hbwe.book.service.NoticeService;
import com.edu.hbwe.book.service.ProductService;
import com.edu.hbwe.book.service.impl.NoticeServiceImpl;
import com.edu.hbwe.book.service.impl.ProductServiceImpl;


@WebServlet("/ShowIndexServlet")
public class ShowIndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	NoticeService nService = new NoticeServiceImpl();
	ProductService pService = new ProductServiceImpl();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//查询公告
		Notice notice = nService.getRecentNotice();
		request.setAttribute("n", notice);
		//热卖商品
		List<Object[]> pList =  pService.getWeekHotProduct();
		request.setAttribute("pList", pList);
		request.getRequestDispatcher("/client/index.jsp").forward(request, response);
	}

}
