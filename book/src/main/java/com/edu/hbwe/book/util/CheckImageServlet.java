package com.edu.hbwe.book.util;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 验证码生成工具类
 */
@WebServlet("/CheckImageServlet")
public class CheckImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	   response.setContentType("image/jpeg;utf-8");
	   //1.获取验证码图片
		VerifyCode vf=new VerifyCode();
		
	   BufferedImage image=vf.getImage();
	 //2.得到四个随机数
	   String code=vf.text();
	   //3.校验验证码
	   request.getSession().setAttribute("serverCode", code);
	   //4.把验证码输入到页面
	   VerifyCode.output(image,response.getOutputStream());
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
