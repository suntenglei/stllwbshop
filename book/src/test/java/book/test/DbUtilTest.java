package book.test;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.apache.commons.dbutils.QueryRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.edu.hbwe.book.entity.Product;
import com.edu.hbwe.book.util.DbUtil;


public class DbUtilTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetDataSource() {
		Product p = new Product();
		p.setId("048e99dd-0340-4681-befa-98240c6177bz");
		p.setName("空的so");
		p.setPrice(25.1);
		p.setCategory("学术");
		p.setPnum(30);
		p.setImgurl("/productImg/14/9/46aa32fa-af48-4616-b2df-12533f0b146e");
		p.setDescription("134646444首都华府]");
		String sql = "insert into products values(?,?,?,?,?,?,?)";
		QueryRunner runner = new QueryRunner(DbUtil.getDataSource());
		System.out.println(p.toString());
		try {
			runner.update(sql, p.getId(), p.getName(), p.getPrice(),
					p.getCategory(), p.getPnum(), p.getImgurl(), p.getDescription());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
